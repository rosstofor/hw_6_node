class UserController {
  getAllUsers(userList) {
    return function (req, res) {
      res.setHeader('Content-Type', 'application/json')
      res.statusCode = 200
      res.end(JSON.stringify(userList))
    }
  }
  createUser(userList) {
    return function (req, res) {
      const lastUserVals = Object.values(userList[userList.length - 1])
      let createdUser = JSON.parse(req.body)
      let newUser = {
        id: lastUserVals[0] + 1,
        name: createdUser.name,
        age: parseInt(createdUser.age),
      }
      userList.push(newUser)
      res.setHeader('Content-Type', 'application/json')
      res.statusCode = 201
      res.end(JSON.stringify(newUser))
    }
  }
  deleteUser(userList) {
    return function (req, res) {
      let el = userList.map((user) => user.id).indexOf(req.body)
      //   console.log(el)
      if (el > -1) userList.splice(el, 1)

      // let el = userList.find((user) => user.id === req.body).id
      // console.log(el)
      // userList.splice(el, 1)
      res.setHeader('Content-Type', 'application/json')
      res.statusCode = 204
      res.end(JSON.stringify(userList))
    }
  }

  optionsUser(req, res) {
    res.setHeader('Content-Type', 'application/json')
    res.statusCode = 200
    res.end()
  }
}

module.exports = new UserController()
